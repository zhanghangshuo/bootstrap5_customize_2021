# Bootstrap5编译

这节课我们来一起定制Bootstrap5

你将会学到：

- sass基础
- 认识bootstrap5源码及其结构
- 根据自己的色板（color plate)、尺寸系统(space & sizing system)更改bootstrap的底层类（utility class)
- 定制bootstrap组件
- 和更多 ....

如果你觉得本课程有帮助，请帮我点赞、投币、收藏一键三连，爱你哦~